
var Sequelize = require("sequelize");

module.exports = function (database) {
    var user =
        database.define("user", {
            name: {type: Sequelize.STRING},
            email: {type: Sequelize.STRING},
            contact: {type: Sequelize.STRING},
            gender: {type: Sequelize.STRING},

        }, {
            timestamps: true
        });

    return user;
};

