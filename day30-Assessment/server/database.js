/*
  File: database.js in the root folder

  Define:
   1. Database Connection
   2. The Data Models - Database Table
 */

var Sequelize = require('sequelize');
var config = require('./config');

//conn equiv now database
var database = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password, {
        host: config.mysql.host,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var skillsModel = require('./model/skills.model')(database);
var userModel = require('./model/user.model')(database);


//skillsModel.hasOne(skillsModel, {foreignKey: 'creator_id'});
userModel.hasMany(skillsModel, {ForeignKey: 'user_id'})

//module.exports = {
  //  usersTable: userModel,
    //skills_future_costsTable: skillsModel


