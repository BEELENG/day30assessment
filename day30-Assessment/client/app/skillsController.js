
'use strict';

(function () {
    angular
        .module("TalentApp")
        .controller("SkillsCtrl", SkillsCtrl);

    SkillsCtrl.$inject = ["SkillsServices", "$stateParams"];

    function SkillsCtrl(SkillsServices, $stateParams) {
        var vm = this;

        vm.userId = $stateParams.user_id;
        console.log("Login user at skills controller: ", $stateParams);

        vm.skills = [];
        vm.skills = {};
        vm.cloudURL = "";

        var defaultSkills = {
            id: 0,
            url: "",
            creator_name: "",
            coursetext: "",
            courseblock: "",
            coursecost: "",
            comments: ""
        };

        function mapData(serverData) {
            serverData.forEach(function (item) {

                vm.skills.push({
                    id: item.id,
                    url: vm.cloudURL + item.url,
                    user_name: item.user.name,


                    skills_upload: item.skills_upload,
                    comments: item.comments
                })
            })
        }

        function initData() {
            SkillServices.getSkillCloudURL()
                .then(function (result) {
                    vm.cloudURL = result.url;
                });

            SkillServices.ListSkills()
                .then(function (result) {
                    mapData(result);
                })
                .catch(function (result) {
                    console.log("List Skill Error ", result);
                });
        }


        // To initialize the vm.skills with database Skills
        initData();

        //Adding Interactive
        vm.clickupdates = function (skillIndex) {
            vm.skills[skillIndex].skills_update++;

            SkillServices.uploadSkillsdetail({
                user_id: vm.skills[skillIndex].id,
                skill_likes: vm.skills[skillIndex].skill_uploads
            })
                .then(function (result) {
                    //console.log("Upload skills", result);
                })
                .catch(function (result) {
                    console.log("Update Error ", result);
                });

        }
    }
})();
