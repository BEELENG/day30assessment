'use strict';

(function () {
    angular
        .module("TalentFutureApp")
        .controller("UserCtrl", MyCtrl);

    MyCtrl.$inject = ["UserServices", "$stateParams"];

    function MyCtrl(UserServices, $stateParams) {
        var myCtrl = this;

        myCtrl.userId = $stateParams.user_id;
        console.log("Login user at user controller: ", myCtrl.userId);

        // var defaultUserObject = {
        myCtrl.userObject = {
            name: "",
            email: "",
            contact: "",
            gender: "",
        };

        myCtrl.getUserName = "";
        myCtrl.selectUser = "";
        myCtrl.server_response = "";

        //myCtrl.userObject = angular.copy(defaultUserObject);
        myCtrl.users = [];

        function initData() {
            UserServices.listusers()
                .then(function (result) {
                    console.log("user list");
                    console.log(result);
                    myCtrl.users = result;
                })
                .catch(function (err) {
                    console.log("error");
                });
        }

        initData();

        myCtrl.displayUserlist = function displayUserlist(user) {
            console.log("Userlist" + user.userlist);
            if (user.userlist == 1) {
                return "Yes, join as a TalentFuture member";
            } else if (user.userlist == 2) {
                return "No, thanks";
            } else {
                return "Pending";
            }
        }

        myCtrl.registerUser = function registerUser() {
            console.log("Sending over");
            console.log(myCtrl.userObject);
            UserServices.addUser(myCtrl.userObject)
                .then(function (result) {
                    myCtrl.server_response = result;

                })
                .catch(function (result) {
                    myCtrl.server_response = result;
                });

            // myCtrl.userObject = angular.copy(defaultUserObject);
        };

        myCtrl.getUser = function getUser() {
            console.log("get user called: " + myCtrl.getUsertName);
            UserServices.getUser(myCtrl.getUserName)
                .then(function (result) {
                    console.log("results : " + JSON.stringify(myCtrl.users));
                    myCtrl.users = result;
                })
                .catch(function (error) {
                    console.log(error);
                });
        };
    }
})();

