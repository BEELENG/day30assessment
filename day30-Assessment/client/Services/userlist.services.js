//userList.js - ie app.service.js file

'use strict';

(function () {
    angular
        .module("TalentApp")
        .service("userListService", userListService);

    userListService.$inject = ["$http", "$q"];

    function userListService($http, $q) {

        var vm = this;

        vm.getUserInfo = getUserInfo;
        vm.updateUserInfo = updateUserInfo;

        function getUserInfo(userId) {
            var deferred = $q.defer();
            console.log("calling REST API to get user record", userId);
            $http
                .get("/api/user/" + userId)
                .then(function (response) {
                    console.log("success http get user record", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                console.log("error http user record", response);
                deferred.reject(response);
            });
            return deferred.promise;
        }

        function updateUserInfo(dataObject) {
            var deferred = $q.defer();
            console.log("calling REST API to update user record",dataObject);
            $http
                .put("/api/user/", dataObject)
                .then(function (response) {
                    console.log("success http put user record", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                console.log("error http user update", response);
                deferred.reject(response);
            });
            return deferred.promise;
        }
    }
})();